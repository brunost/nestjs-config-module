<h1 align="center"></h1>

<div align="center">
  <a href="http://nestjs.com/" target="_blank">
    <img src="https://nestjs.com/img/logo_text.svg" width="150" alt="Nest Logo" />
  </a>
</div>

<h3 align="center">NestJS Environment Config Module</h3>

<div align="center">
  <a href="https://nestjs.com" target="_blank">
    <img src="https://img.shields.io/badge/built%20with-NestJs-red.svg" alt="Built with NestJS">
  </a>
</div>

## Installation
```bash
npm i -S @grouse/nestjs-config-module
```
## Usage
This module expects there to exist an enviroment file in the root of the consuming projects folder.

If NODE_ENV is not set it will default to "development.env". 

## Example
### development.env
```env
WELCOME_MESSAGE="Hello World!"
```
### app.module.ts
```typescript
// root module
import { ConfigModule } from "@grouse/nestjs-config-module";

@Module({
  imports: [ConfigModule],
})
export class AppModule {}
```
### app.service.ts
```typescript
// consumer
import { ConfigService } from "@grouse/nestjs-config-module";

@Injectable()
export class AppService {

  constructor(
    private readonly config: ConfigService,
  ) {}

  public getHello(): string {
    return this.config.get("WELCOME_MESSAGE");
  }

}

```
# License

Licensed under the MIT License - see the [LICENSE](LICENSE) file for details.